﻿#include <iostream>
#include <time.h>

using namespace std;

int main() 
{
    int day = 12;
    int sum = 0;
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++) 
    {
        for (int j = 0; j < N; j++) 
        {
            array[i][j] = i + j;
            cout << array[i][j];
        }
        cout << '\n';
    }

    int index = day % N;
    for (int j = 0; j < N; j++) 
    {
        sum += array[index][j];
    }

    cout << "Division remainder " << index << '\n';
    cout << "Sum of elements in a row " << sum << '\n';
}